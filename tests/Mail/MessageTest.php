<?php

namespace Ibexcore\Api\Tests;

use Illuminate\Support\Facades\View;

class MessageTest extends \TestCase
{
    /**
     * @var \Ibexcore\Api\Mail\Message
     */
    protected $message;

    public function setUp()
    {
        parent::setUp();
        $this->message = $this->app->make('Ibexcore\Api\Mail\Message');
    }

    /**
     * @expectedException \Ibexcore\Api\Exceptions\InvalidOptionException
     */
    public function test_validate_email_address_fail_missing_email()
    {
        $this->message->validateEmailAddress(null);
    }

    /**
     * @expectedException \Ibexcore\Api\Exceptions\InvalidOptionException
     */
    public function test_validate_email_address_invalid_address()
    {
        $this->message->validateEmailAddress('aaa');
    }

    public function test_validate_email_address()
    {
        $this->assertTrue($this->message->validateEmailAddress('aa@lol.com'));
    }

    public function test_is_valid()
    {
        $this->assertNotTrue($this->message->isValid());
    }

    public function test_is_valid_with_from()
    {
        $this->message->to('aa@lol.com', 'aa');
        $this->assertNotTrue($this->message->isValid());
    }

    public function test_is_valid_with_from_with_subject()
    {
        $this->message->to('aa@lol.com', 'aa');
        $this->message->subject('aa');
        $this->assertNotTrue($this->message->isValid());
    }

    public function test_is_valid_without_subject()
    {
        $this->message->to('aa@lo.com', 'aa');
        $this->message->body('aa');

        $this->assertNotTrue($this->message->isValid());
    }

    public function test_is_valid_valid()
    {
        $this->message->to('aa@lol.com', 'aa');
        $this->message->subject('aa');
        $this->message->body('aaa');
        $this->assertTrue($this->message->isValid());
    }

    /**
     * @expectedException  \Ibexcore\Api\Exceptions\InvalidOptionException
     */
    public function test_set_from_invalid_email()
    {
        $this->message->from('aa');
    }

    public function test_set_from()
    {
        $this->message->from('aa@lol.com', 'aa');
        $this->assertEquals([
            'address' => 'aa@lol.com',
            'name' => 'aa'
        ], $this->message->getFrom());
    }

    public function test_view()
    {
        $this->app->withFacades();
        View::shouldReceive('make')->once()->with('some_view', [], [])->andReturn('aa');
        $this->message->view('some_view');

        $this->assertEquals('aa', $this->message->getBody());
    }
}
