<?php

namespace Ibexcore\Api\Tests\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use Mockery;

// todo: check that the request exceptions are throwing proper messages
// todo: check that the InvalidRequestExceptions are throwing proper messages
// todo: while this does work in test, it needs manual checking!

class GuzzleTestWithGuzzleMocksTest extends \TestCase
{
    protected $client;

    /**
     * @var \Ibexcore\Api\Request\Guzzle
     */
    protected $guzzle;

    /**
     * @var Mockery\Mock
     */
    protected $request;

    public function setUp()
    {
        parent::setUp();
        $this->request = Mockery::mock('Ibexcore\Api\Request\Request');
        $this->guzzle = $this->app->make('Ibexcore\Api\Request\Guzzle')
            ->setRequest($this->request);
    }

    public function setUpClient(array $response)
    {
        /*$mock = new MockHandler([
            new Response(200, ['X-Foo' => 'Bar']),
            new Response(202, ['Content-Length' => 0]),
            new RequestException("Error Communicating with Server", new Request('GET', 'test'))
        ]);*/

        $mock = new MockHandler($response);


        $handler = HandlerStack::create($mock);
        $this->client = new Client(['handler' => $handler]);

        $this->guzzle->setClient($this->client);
    }

    /**
     * @expectedException \Ibexcore\Api\Exceptions\RequestException
     */
    public function test_request_failed_request()
    {
        $this->setUpClient([
            new RequestException("Some error", new Request('GET', 'test'))
        ]);

        $this->request->shouldReceive('buildUrl')->once()->with('test')->andReturn('aa');
        $this->request->shouldReceive('getKey')->once()->andReturn('12');
        $this->request->shouldReceive('getSecret')->once()->andReturn('34');

        $this->guzzle->get('test', 'r');
    }

    /**
     * @expectedException \Ibexcore\Api\Exceptions\RequestException
     */
    public function test_request_not_http_response_200()
    {
        $this->setUpClient([
            new Response(500, [], json_encode(['data' => ['a' => 'y']]))
        ]);

        $this->request->shouldReceive('buildUrl')->once()->with('test')->andReturn('aa');
        $this->request->shouldReceive('getKey')->once()->andReturn('12');
        $this->request->shouldReceive('getSecret')->once()->andReturn('34');

        $this->guzzle->get('test', 'a');
    }

    /**
     * @expectedException \Ibexcore\Api\Exceptions\InvalidResponseException
     */
    public function test_request_not_valid_json()
    {
        $this->setUpClient([
            new Response(200, [], 'aaa')
        ]);

        $this->request->shouldReceive('buildUrl')->once()->with('test')->andReturn('aa');
        $this->request->shouldReceive('getKey')->once()->andReturn('12');
        $this->request->shouldReceive('getSecret')->once()->andReturn('34');

        $this->guzzle->get('test', 'a');
    }

    /**
     * @expectedException \Ibexcore\Api\Exceptions\InvalidResponseException
     */
    public function test_request_missing_data_key()
    {
        $this->setUpClient([
            new Response(200, [], json_encode(['bad_key' => ['a' => 'b']]))
        ]);

        $this->request->shouldReceive('buildUrl')->once()->with('test')->andReturn('aa');
        $this->request->shouldReceive('getKey')->once()->andReturn('12');
        $this->request->shouldReceive('getSecret')->once()->andReturn('34');

        $this->guzzle->get('test', 'a');
    }

    /**
     * @expectedException \Ibexcore\Api\Exceptions\InvalidResponseException
     */
    public function test_request_missing_response_variable()
    {
        $this->setUpClient([
            new Response(200, [], json_encode(['data' => ['a' => 'b']]))
        ]);

        $this->request->shouldReceive('buildUrl')->once()->with('test')->andReturn('aa');
        $this->request->shouldReceive('getKey')->once()->andReturn('12');
        $this->request->shouldReceive('getSecret')->once()->andReturn('34');

        $this->guzzle->get('test', 'b');
    }

    public function test_request()
    {
        $this->setUpClient([
            new Response(200, [], json_encode(['data' => ['a' => 'b']]))
        ]);

        $this->request->shouldReceive('buildUrl')->once()->with('test')->andReturn('aa');
        $this->request->shouldReceive('getKey')->once()->andReturn('12');
        $this->request->shouldReceive('getSecret')->once()->andReturn('34');

        $this->assertEquals('b', $this->guzzle->get('test', 'a'));
    }
}