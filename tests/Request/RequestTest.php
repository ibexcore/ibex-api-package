<?php

namespace Ibexcore\Api\Tests\Request;

use Mockery;

class RequestTest extends \TestCase
{
    /**
     * @var Mockery\Mock
     */
    protected $config;

    /**
     * @var \Ibexcore\Api\Request\Request
     */
    protected $request;

    public function setUp()
    {
        parent::setUp();
        $this->config = Mockery::mock('Illuminate\Contracts\Config\Repository');
        $this->request = $this->app->make('Ibexcore\Api\Request\Request')
            ->setConfig($this->config);
    }


    public function test_build_url()
    {
        $this->config->shouldReceive('get')->once()->with('ibexapi.url')->andReturn('url.com');
        $this->assertEquals('url.com/a', $this->request->buildUrl('a'));
    }

    /**
     * @expectedException \Ibexcore\Api\Exceptions\InvalidConfigException
     */
    public function test_build_url_throw()
    {
        $this->config->shouldReceive('get')->once()->with('ibexapi.url')->andReturn(null);
        $this->request->buildUrl('a');
    }


    public function test_build_url_with_slashes()
    {
        $this->config->shouldReceive('get')->once()->with('ibexapi.url')->andReturn('url.com/');
        $this->assertEquals('url.com/a', $this->request->buildUrl('/a'));
    }

    public function test_get_key()
    {
        $this->config->shouldReceive('get')->once()->with('ibexapi.key')->andReturn('12');
        $this->assertEquals('12', $this->request->getKey());
    }

    /**
     * @expectedException \Ibexcore\Api\Exceptions\InvalidConfigException
     */
    public function test_get_key_not_string()
    {
        $this->config->shouldReceive('get')->once()->with('ibexapi.key')->andReturn([]);
        $this->request->getKey();
    }

    public function test_get_secret()
    {
        $this->config->shouldReceive('get')->once()->with('ibexapi.secret')->andReturn('12');
        $this->assertEquals('12', $this->request->getSecret());
    }

    /**
     * @expectedException \Ibexcore\Api\Exceptions\InvalidConfigException
     */
    public function test_get_secret_not_string()
    {
        $this->config->shouldReceive('get')->once()->with('ibexapi.secret')->andReturn([]);
        $this->request->getSecret();
    }
}
