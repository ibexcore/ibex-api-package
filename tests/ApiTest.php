<?php

namespace Ibexcore\Api\Tests;

use Mockery;

class ApiTest extends \TestCase
{
    /**
     * @var \Ibexcore\Api\Api
     */
    protected $api;

    /**
     * @var Mockery\Mock
     */
    protected $request;

    public function setUp()
    {
        parent::setUp();
        $this->request = Mockery::mock('Ibexcore\Api\Contracts\Request');
        $this->api = $this->app->make('Ibexcore\Api\Api')
            ->setRequest($this->request);
    }

    public function test_send_mail()
    {
        $this->request->shouldReceive('post')->once()->with('mail', 'queued', [
            'to' => [
                ['email' => 'a@lol.com', 'name' => ''],
                ['email' => 'b@lol.com', 'aa']
            ],
            'subject' => 'Test',
            'body' => 'aaaa'
        ]);

        $this->api->mail(function ($message) {
            /**
             * @var \Ibexcore\Api\Mail\Message $message
             */
            $message->to('a@lol.com');
            $message->to('b@lol.com', 'aa');
            $message->subject('Test');
            $message->body('aaaa');
        });
    }
}