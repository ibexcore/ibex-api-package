<?php

return [
    /*
     * The base url of the API
     */
    'url' => 'https://api.ibexcore.com/v1',

    /*
     * Your API key
     */
    'key' => env('IBEXAPI_KEY', null),

    /*
     * Your API secret
     */
    'secret' => env('IBEXAPI_SECRET', null),

    /*
     * Request options
     */
    'request' => [
        /*
         * What driver to use.
         * You can either specify a class in the `request` folder (ibexcore/ibexapi/src/Request) or use a
         * fully-qualified class name (e.g. \App\Request\Request::class)
         */
        'driver' => 'guzzle',

        /*
         * Any extra CURL headers you want to add
         */
        'headers' => []
    ],

    'api' => [
        /*
         * What driver to use.
         * You can either specify a class in the `request` folder (ibexcore/ibexapi/src/Api) or use a
         * fully-qualified class name (e.g. \App\Api\Api::class)
         */
        'driver' => 'api'
    ]
];
