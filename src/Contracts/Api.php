<?php

namespace Ibexcore\Api\Contracts;

use Closure;

interface Api
{
    /**
     * Send a message. The variable passed into your closure is Ibexcore\Ibexapi\Mail\Message
     *
     * @param Closure $message
     * @return bool
     */
    public function mail(Closure $message);
}
