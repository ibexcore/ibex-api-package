<?php

namespace Ibexcore\Api\Contracts;

/**
 * Interface Request
 * @package Ibexcore\Api\Contracts
 */
interface Request
{
    /**
     * Do a POST request with the extra key and secret headers added automatically.
     *
     * @param string $url
     * @param string $responseVariable - what variable to check for in the response
     * @param array $data
     * @param array $headers - any extra headers to add on
     * @return mixed
     */
    public function post($url, $responseVariable, $data = [], $headers = []);

    /**
     * Do a GET request with the extra key and secret headers added automatically.
     *
     * @param string $url
     * @param string $responseVariable - what variable to check for in the response
     * @param array $headers - any extra headers to add on
     * @return mixed
     */
    public function get($url, $responseVariable, $headers = []);

    /**
     * Do a PUT request with the extra key and secret headers added automatically.
     *
     * @param string $url
     * @param string $responseVariable - what variable to check for in the response
     * @param array $data
     * @param array $headers - any extra headers to add on
     * @return mixed
     */
    public function put($url, $responseVariable, $data = [], $headers = []);

    /**
     * Do a DELETE request with the extra key and secret headers added automatically.
     *
     * @param string $url
     * @param string $responseVariable - what variable to check for in the response
     * @param array $headers - any extra headers to add on
     * @return mixed
     */
    public function delete($url, $responseVariable, $headers = []);
}
