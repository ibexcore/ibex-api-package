<?php

namespace Ibexcore\Api\Mail;

/**
 * Interface MessageInterface
 * @package Ibexcore\Ibexapi\Mail
 */
interface MessageInterface
{
    /**
     * Add a mail recipient. Can be multiple.
     *
     * @param string $email
     * @param null|string $name
     * @return $this
     */
    public function to($email, $name = null);

    /**
     * Add who this email is from.
     *
     * @param string $email
     * @param null|string $name
     * @return $this
     */
    public function from($email, $name = null);

    /**
     * Set the email subject.
     *
     * @param string $subject
     * @return $this
     */
    public function subject($subject);

    /**
     * Set the email body
     *
     * @param string $body
     * @return $this
     */
    public function body($body);

    /**
     * Use a Laravel/Lumen view for the body.
     * This is an alternative shortcut to using the body syntax ($this->body).
     *
     * @param string $view
     * @param array $data
     * @return $this
     */
    public function view($view, array $data = []);

    /**
     * @return array
     */
    public function getTo();

    /**
     * @return array
     */
    public function getFrom();

    /**
     * @return string
     */
    public function getSubject();

    /**
     * @return string
     */
    public function getBody();

    /**
     * Is the message valid? I.e. does it have a valid to address, body and subject?
     * Returns a string if the message isn't valid with the reason it is not
     * valid.
     *
     * @return true|string
     */
    public function isValid();
}
