<?php

namespace Ibexcore\Api\Mail;

use Ibexcore\Api\Exceptions\InvalidOptionException;
use Illuminate\Validation\ValidationException;

class Message implements MessageInterface
{
    /**
     * @var array
     */
    protected $to = [];

    /**
     * @var array
     */
    protected $from = [];

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var string
     */
    protected $body;

    /**
     * Add who this email is from.
     *
     * @param string $email
     * @param null|string $name
     * @return $this
     * @throws InvalidOptionException
     */
    public function from($email, $name = null)
    {
        $this->validateEmailAddress($email);
        $this->from = ['address' => $email, 'name' => $name];
        return $this;
    }

    /**
     * Add a mail recipient. Can be multiple.
     *
     * @param string $email
     * @param null|string $name
     * @return $this
     * @throws InvalidOptionException
     */
    public function to($email, $name = null)
    {
        $this->validateEmailAddress($email);
        $this->to[] = ['address' => $email, 'name' => $name];
        return $this;
    }

    /**
     * Set the email subject.
     *
     * @param string $subject
     * @return $this
     */
    public function subject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Set the email body
     *
     * @param string $body
     * @return $this
     */
    public function body($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * Use a Laravel/Lumen view for the body.
     * This is an alternative shortcut to using the body syntax ($this->body).
     *
     * @param string $view
     * @param array $data
     * @return $this
     */
    public function view($view, array $data = [])
    {
        $this->body = view($view, $data);
        return $this;
    }

    /**
     * @return array
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return array
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Is the message valid? I.e. does it have a valid to address, body and subject?
     *
     * @return true|string
     */
    public function isValid()
    {
        if  (count($this->to) < 1) {
            return 'There is not enough recipients';
        }

        if (!$this->body) {
            return 'There is not any body content';
        }

        if (!$this->subject) {
            return 'There is no subject';
        }

        return true;
        //return count($this->to) > 0 && $this->body && $this->subject;
    }

    /**
     * @return \Illuminate\Validation\Factory
     */
    public function getValidator()
    {
        return app('validator');
    }

    /**
     * Is an email address valid? Returns true or it throws otherwise.
     *
     * @param string $email
     * @return true
     * @throws InvalidOptionException
     */
    public function validateEmailAddress($email)
    {
        try {
            $this->getValidator()->validate(['email' => $email], ['email' => 'required|email']);
        } catch (ValidationException $e) {
            throw new InvalidOptionException("The email address '{$email}' is not valid");
        }

        return true;
    }
}
