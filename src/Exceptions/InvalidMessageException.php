<?php

namespace Ibexcore\Api\Exceptions;

class InvalidMessageException extends ApiException
{
    public function __construct($reason)
    {
        return parent::__construct($reason);
    }
}
