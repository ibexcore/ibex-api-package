<?php

namespace Ibexcore\Api;

use Closure;
use Ibexcore\Api\Exceptions\InvalidConfigException;
use Ibexcore\Api\Mail\Message;
use Ibexcore\Api\Contracts\Request;
use Ibexcore\Api\Contracts\Api as ApiContract;
use Illuminate\Contracts\Config\Repository as Config;

class Api implements ApiContract
{
    protected $request;

    protected $config;

    public function __construct(Request $request, Config $config)
    {
        $this->request = $request;
        $this->config  = $config; // todo: is this needed?
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * Send a message. The variable passed into your closure is Ibexcore\Ibexapi\Mail\Message
     *
     * @param Closure $message
     * @return bool
     * @throws InvalidConfigException
     */
    public function mail(Closure $message)
    {
        $config = new Message();
        $message($config);

        // check that the message is valid
        if (($reason = $config->isValid()) !== true) {
            throw new InvalidConfigException($reason);
        }

        // Build the post data
        $postData = [
            'to' => [],
            'subject' => $config->getSubject(),
            'body' => $config->getBody()
        ];

        // add the recipients onto the array.
        // todo: change this to array_walk (can't get the docs as working offline)
        foreach ($config->getTo() as $item) {
            $postData['to'][] = [
                'email' => $item['address'],
                'name' => $item['name']
            ];
        }

        // todo: check params and variable names
        // todo: do we want to validate that this is a valid request before sending it? HINT: yes
        return $this->request->post('mail', 'queued', [
            'to' => [
                ['email' => 'a@lol.com', 'name' => ''],
                ['email' => 'b@lol.com', 'aa']
            ],
            'subject' => 'Test',
            'body' => 'aaaa'
        ]);
    }
}
