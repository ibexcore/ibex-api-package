<?php

namespace Ibexcore\Api;

use Illuminate\Support\ServiceProvider;

/**
 * Bootstrap the package for Laravel/Lumen
 *
 * @package Ibexcore\Ibexapi
 */
class IbexapiServiceProvider extends ServiceProvider
{

    public function boot()
    {
        // tell Laravel that this package publishes a config file
        $this->publishes([
            __DIR__.'/config/ibexapi.php' => $this->getConfigPath('ibexapi.php'),
        ]);
    }

    public function register()
    {
        // merge the existing custom config file with the one in this package
        $this->mergeConfigFrom(__DIR__.'/config/ibexapi.php', 'ibexapi');

        // register the contracts
        $requestConfig = config('ibexapi.request.driver');
        $requestClass = class_exists($requestConfig) ? $requestConfig : __NAMESPACE__ . '\Request\\' . ucfirst($requestConfig);

        $this->app->singleton(__NAMESPACE__ . '\Contracts\Api', __NAMESPACE__ . '\Api');
        $this->app->singleton(__NAMESPACE__ . '\Contracts\Request', $requestClass);
    }

    protected function getConfigPath($path)
    {
        if (function_exists('config_path')) {
            return config_path($path);
        }

        return base_path('config/' . ltrim($path, '/'));
    }
}
