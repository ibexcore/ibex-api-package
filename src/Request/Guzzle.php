<?php

namespace Ibexcore\Api\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException as GuzzleRequestException;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;
use Ibexcore\Api\Contracts\Request;
use Ibexcore\Api\Exceptions\InvalidResponseException;
use Ibexcore\Api\Exceptions\RequestException;
use Ibexcore\Api\Request\Request as RequestHelper;

class Guzzle implements Request
{
    protected $client;

    protected $request;

    public function __construct(Client $client, RequestHelper $request)
    {
        $this->client = $client;
        $this->request = $request;
    }

    public function setClient(Client $client)
    {
        $this->client = $client;
        return $this;
    }

    public function setRequest(RequestHelper $request)
    {
        $this->request =  $request;
        return $this;
    }

    /**
     * Do a POST request with the extra key and secret headers added automatically.
     *
     * @param string $url
     * @param string $responseVariable - what variable to check for in the response
     * @param array $data
     * @param array $headers - any extra headers to add on
     * @return mixed
     */
    public function post($url, $responseVariable, $data = [], $headers = [])
    {
        // TODO: Implement post() method.
    }

    /**
     * Do a GET request with the extra key and secret headers added automatically.
     *
     * @param string $path
     * @param string $responseVariable - what variable to check for in the response
     * @param array $headers - any extra headers to add on
     * @return object|array
     * @throws InvalidResponseException
     */
    public function get($path, $responseVariable, $headers = [])
    {
        return $this->request('GET', $this->request->buildUrl($path), $responseVariable, [], $headers);
    }

    /**
     * Do a PUT request with the extra key and secret headers added automatically.
     *
     * @param string $url
     * @param string $responseVariable - what variable to check for in the response
     * @param array $data
     * @param array $headers - any extra headers to add on
     * @return mixed
     */
    public function put($url, $responseVariable, $data = [], $headers = [])
    {
        // TODO: Implement put() method.
    }

    /**
     * Do a DELETE request with the extra key and secret headers added automatically.
     *
     * @param string $url
     * @param string $responseVariable - what variable to check for in the response
     * @param array $headers - any extra headers to add on
     * @return mixed
     */
    public function delete($url, $responseVariable, $headers = [])
    {
        // TODO: Implement delete() method.
    }

    protected function request($method, $url, $responseVariable, $body = [], $headers = [])
    {
        try {
            $request = $this->client->request(strtoupper($method), $url, $this->getRequestOptions($body, $headers));
        } catch (GuzzleRequestException $e) {
            throw new RequestException($e->getMessage(), $e->getCode(), $e);
        }

        return $this->handleRequest($request, $responseVariable);
    }

    protected function handleRequest(Response $request, $responseVariable)
    {
        if ($request->getStatusCode() == 200) {
            $body = json_decode($request->getBody());

            if (json_last_error() === JSON_ERROR_NONE) {
                if (isset($body->data)) {
                    if (isset($body->data->$responseVariable)) {
                        return $body->data->$responseVariable;
                    }

                    throw new InvalidResponseException("Response variable {$responseVariable} was missing from the response", $request);
                }
                throw new InvalidResponseException("The data response variable was missing", $request);
            }

            throw new InvalidResponseException("The returned JSON was not valid", $request);
        }

        throw new InvalidResponseException("Returned HTTP code was not 200", $request);
    }

    protected function getRequestOptions($body = [], $extraHeaders = [])
    {
        $defaultHeaders = [
            'key' => $this->request->getKey(),
            'secret' => $this->request->getSecret()
        ];
        $options = [
            'headers' => array_replace($defaultHeaders, $extraHeaders)
        ];

        if ($body) {
            $options['form_params'] = $body;
        }
        return $options;
    }
}
