<?php

namespace Ibexcore\Api\Request;

use Ibexcore\Api\Exceptions\InvalidConfigException;
use Ibexcore\Api\Exceptions\InvalidOptionException;
use Illuminate\Contracts\Config\Repository as Config;

/**
 * While you can extend this class from your request driver, I'd recommend
 * injecting it as it can be a pain having to have all of this classes
 * dependencies in your drivers construct.
 *
 * @package Ibexcore\Api\Request
 */
class Request
{
    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function setConfig(Config $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * Build a url for the API
     *
     * @param string $path
     * @return string
     */
    public function buildUrl($path = '/')
    {
        return rtrim($this->getOrThrow('ibexapi.url', 'string'), '/') . '/' . ltrim($path, '/');
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function getKey()
    {
        /*$key = $this->getOrThrow('ibexapi.key');

        if (is_string($key)) {
            return $key;
        }

        throw new InvalidConfigException("The Ibexcore API key is not a string");*/
        return $this->getOrThrow('ibexapi.key');
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function getSecret()
    {
        /*$secret = $this->getOrThrow('ibexapi.secret');

        if (is_string($secret)) {
            return $secret;
        }

        throw new InvalidConfigException("The Ibexcore API secret is not a string");*/
        return $this->getOrThrow('ibexapi.secret', 'string');
    }

    /**
     * @param string $key
     * @param bool|string $type - The type checking to do. Rudimentary validation.
     * @return mixed
     * @throws InvalidConfigException
     * @throws InvalidOptionException
     */
    public function getOrThrow($key, $type = false)
    {
        if (($value = $this->config->get($key)) != null) {
            if ($type) {
                switch ($type) {
                    case 'string':
                        $check = function ($value) { return is_string($value);};
                        break;
                    default:
                        throw new InvalidOptionException("The type {$type} is not supported");
                }

                if ($check($value)) {
                    return $value;
                }

                throw new InvalidConfigException("The key {$key} is not a {$type}");

            }
            return $value;
        }

        throw new InvalidConfigException("The key {$key} was null from the config");
    }
}