# Ibexcore API package

Ibexcore API package for Laravel/Lumen.

## Installation

#### Install the package

`composer require ibexcore/ibexapi`

#### Register the service provider

##### Laravel

Add the service provider in your `config/app.php`

    'providers' => [
        Ibexcore\Ibexapi\IbexapiServiceProvider::class
    ]
    
##### Lumen

In your `bootstrap/app.php` add the following:

`$app->register(Ibexcore\Ibexapi\IbexapiServiceProvider::class);`

#### Update your .env file

Add `IBEXAPI_KEY` and `IBEXAPI_SECRET` to your key and secret.

## Exceptions

All exceptions extend `\Ibexcore\Api\Exceptions\ApiException`. This exception
is never thrown directly.

## Planning

/Api.php - Handles all of the API interaction
/Contracts/Ibexapi - Contract for api.php. Why is there a contract? So there is succint documentation if needed.
                     If we ever decided to change how the API talks to the server, we could change swap it
                     out in IbexapiServiceProvider
/Contracts/Request - Request contract
/Request/Guzzle - The guzzle implementation of the request contract.
/Request/Request - The base class of the request drivers.

## TODO

* Write guzzle tests (using guzzle mocks) (DONE)
* Test that the guzzle driver works
* Write guzzle test (unit - after all guzzle mocks have been written)
* Write API tests
* Pull in ibexcore alert factory
* Include ng-route (for links to pages with more details about the website in showcase/work)

### Ibex API

* Add test get route (to confirm key and secret are working correctly)
* Add ?fake=true to **all** routes to not actually do anything for functional tests
* Document API on Apiary? Maybe?